variable "owner" {
  default = "Serhii Bodnia"
}

variable "aws_name" {
  default = "Terraform"
}

variable "network" {
  default = "10.0"
}

variable "cidr_block" {
    default = "10.0.0.0/16"
}

variable "ami" {
  default = "ami-0194c3e07668a7e36"
}

variable "i_type" {
  default = "t2.micro"
}

variable "rds_engine" {
  default = "postgres"
}

variable "rds_engine_v" {
  default = "13.3"
}

variable "rds_port" {
  default = "5432"
}

variable "rds_i_class" {
  default = "db.t3.micro" 
}

variable "rds_az" {
  default = "eu-west-2a"
}