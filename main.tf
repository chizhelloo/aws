provider "aws" {
  region = "eu-west-2"
}
# VIRTUAL PRIVATE CLOUD
# ---------------------

resource "aws_vpc" "vpc" {
  cidr_block = "${var.cidr_block}"
  tags = {
    Name  = "${var.aws_name}"
    Owner = "${var.owner}"
  }
}

# Subnets

resource "aws_subnet" "public_a" {
  vpc_id               = "${aws_vpc.vpc.id}"
  cidr_block           = "${var.network}.11.0/24"
  availability_zone_id = "euw2-az2"
  tags = {
    Name  = "${var.aws_name} Public A"
    Owner = "${var.owner}"
  }
}

resource "aws_subnet" "public_b" {
  vpc_id               = "${aws_vpc.vpc.id}"
  cidr_block           = "${var.network}.12.0/24"
  availability_zone_id = "euw2-az3"
  tags = {
    Name  = "${var.aws_name} Public B"
    Owner = "${var.owner}"
  }
}

resource "aws_subnet" "private_a" {
  vpc_id               = "${aws_vpc.vpc.id}"
  cidr_block           = "${var.network}.21.0/24"
  availability_zone_id = "euw2-az2"
  tags = {
    Name  = "${var.aws_name} Private A"
    Owner = "${var.owner}"
  }
}

resource "aws_subnet" "private_b" {
  vpc_id               = "${aws_vpc.vpc.id}"
  cidr_block           = "${var.network}.22.0/24"
  availability_zone_id = "euw2-az3"
  tags = {
    Name  = "${var.aws_name} Private B"
    Owner = "${var.owner}"
  }
}

#Route Tables

resource "aws_route_table" "public_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name  = "${var.aws_name} PublicRouteTable"
    Owner = "${var.owner}"
  }
}

resource "aws_route_table" "private_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name  = "${var.aws_name} PrivateRouteTable"
    Owner = "${var.owner}"
  }
}

#InternetGateways

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name  = "${var.aws_name} IGW"
    Owner = "${var.owner}"
  }
}

#Elastic IP addresses

resource "aws_eip" "eip_a" {
  tags = {
    Name  = "${var.aws_name} Elastic IP - A"
    Owner = "${var.owner}"
  }
}

resource "aws_eip" "eip_b" {
  tags = {
    Name  = "${var.aws_name} Elastic IP - B"
    Owner = "${var.owner}"
  }
}

#NAT Gateways 

resource "aws_nat_gateway" "nat_a" {
  subnet_id     = "${aws_subnet.private_a.id}"
  allocation_id = "${aws_eip.eip_a.id}"
  tags = {
    Name  = "${var.aws_name} NAT - A"
    Owner = "${var.owner}"
  }
}

resource "aws_nat_gateway" "nat_b" {
  subnet_id     = "${aws_subnet.private_b.id}"
  allocation_id = "${aws_eip.eip_b.id}"
  tags = {
    Name  = "${var.aws_name} NAT - B"
    Owner = "${var.owner}"
  }
}

# SECURITY
# --------

# Security Groups



# EC2
# ---

# Key pair 

resource "tls_private_key" "rsa" {
  algorithm = "RSA"
  rsa_bits  = "4096"
}

resource "aws_key_pair" "key" {
  key_name = "${var.aws_name}"
  public_key = "${tls_private_key.rsa.public_key_openssh}"
}


# Instances

data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  owners = ["099720109477"]
}

resource "aws_instance" "bastion" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "t2.micro"
  key_name      = "${var.aws_name}"
  tags = {
    Name  = "BastionServer"
    Owner = "${var.owner}"
  }
}

resource "aws_instance" "mongo_primary" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.i_type}"
  key_name      = "${var.aws_name}"
  tags = {
    Name  = "${var.aws_name} MongoDB (primary)"
    Owner = "${var.owner}"
  }
}

resource "aws_instance" "mongo_secondary_a" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.i_type}"
  key_name      = "${var.aws_name}"
  tags = {
    Name  = "${var.aws_name} MongoDB (secondary1)"
    Owner = "${var.owner}"
  }
}

resource "aws_instance" "mongo_secondary_b" {
  ami           = "${data.aws_ami.ubuntu.id}"
  instance_type = "${var.i_type}"
  key_name      = "${var.aws_name}"
  tags = {
    Name  = "${var.aws_name} MongoDB (secondary2)"
    Owner = "${var.owner}"
  }
}

# Volumes

resource "aws_ebs_volume" "mongo_prim" {
  availability_zone = "eu-west-2b"
  size              = "40"
  tags = {
    Name  = "${var.aws_name} MongoDB"
    Owner = "${var.owner}"
  }
}

resource "aws_ebs_volume" "mongo_sec_a" {
  availability_zone = "eu-west-2b"
  size              = "40"
  tags = {
    Name  = "${var.aws_name} MongoDB"
    Owner = "${var.owner}"
  }
}

resource "aws_ebs_volume" "mongo_sec_b" {
  availability_zone = "eu-west-2b"
  size              = "40"
  tags = {
    Name  = "${var.aws_name} MongoDB"
    Owner = "${var.owner}"
  }
}

resource "aws_ebs_volume" "bast" {
  availability_zone = "eu-west-2a"
  size              = "40"
  tags = {
    Name  = "Bastion"
    Owner = "${var.owner}"
  }
}

# RDS

resource "aws_db_instance" "rds" {
  instance_class               = "${var.rds_i_class}"
  availability_zone            = "${var.rds_az}"
  allocated_storage            = 20
  engine                       = "${var.rds_engine}"
  engine_version               = "${var.rds_engine_v}"
  username                     = "terraform"
  password                     = "54324428"
  port                         = "${var.rds_port}"
  performance_insights_enabled = true
  storage_encrypted            = "true"
  copy_tags_to_snapshot        = true
}