aws_name = "Terraform"
owner = "Serhii Bodnia"

# VPC
cidr_block = "10.0.0.0/16"

# EC2
ami = "ami-0194c3e07668a7e36"
i_type = "t2.micro"

# RDS 
rds_engine = "postgres"
rds_engine_v = "13.3"
rds_port = "5432"
rds_i_type =  "db.t3.micro"
rds_az = "eu-west-2a"